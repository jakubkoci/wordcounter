package com.jakubkoci.wordcounter;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * @author kuba
 */
public class WordCounterTest {

    @Test
    public void shouldReturn3WordsAsUniqueKeysInMap() {
        WordCounter wordCounter = new WordCounter();
        Map<String, Integer> words = wordCounter.getWordsWithCount("a b a c");
        Assert.assertEquals(3, words.keySet().size());
    }

    @Test
    public void shouldReturn1WordWithCount2() {
        WordCounter wordCounter = new WordCounter();
        Map<String, Integer> words = wordCounter.getWordsWithCount("a b a c");
        Assert.assertEquals(2, words.get("a").intValue());
    }

    @Test
    public void shouldReturn2WordsWithCount1() {
        WordCounter wordCounter = new WordCounter();
        Map<String, Integer> words = wordCounter.getWordsWithCount("a b a c");
        Assert.assertEquals(1, words.get("b").intValue());
        Assert.assertEquals(1, words.get("c").intValue());
    }
}
