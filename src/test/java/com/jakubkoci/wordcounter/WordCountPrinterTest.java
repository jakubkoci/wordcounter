package com.jakubkoci.wordcounter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kuba
 */
public class WordCountPrinterTest {

    private ByteArrayOutputStream testOutput;
    private PrintStream testPrintStream;
    private WordCountPrinter printer;

    @Before
    public void setUpOutputStreamAndPrinter() {
        testOutput = new ByteArrayOutputStream();
        testPrintStream = new PrintStream(testOutput);
        printer = new WordCountPrinter(testPrintStream);
    }

    @Test
    public void shouldPrintOutputOrderedByKey() throws Exception {
        Map<String, Integer> wordsWithCount = new HashMap<String, Integer>();
        wordsWithCount.put("b", 1);
        wordsWithCount.put("a", 2);
        wordsWithCount.put("c", 1);

        printer.print(wordsWithCount);

        Assert.assertEquals("a: 2\nb: 1\nc: 1\n", testOutput.toString());
    }

    @Test
    public void shouldPrintOutputOrderedByValue() throws Exception {
        Map<String, Integer> wordsWithCount = new HashMap<String, Integer>();
        wordsWithCount.put("a", 2);
        wordsWithCount.put("b", 3);
        wordsWithCount.put("c", 1);

        printer.print(wordsWithCount);

        Assert.assertEquals("b: 3\na: 2\nc: 1\n", testOutput.toString());
    }
}
