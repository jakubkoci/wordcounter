package com.jakubkoci.wordcounter;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author kuba
 */
public class WordCountPrinter {

    private PrintStream outputStream;

    public WordCountPrinter(PrintStream outputStream) {
        this.outputStream = outputStream;
    }

    public void print(Map<String, Integer> wordsWithCount) {
        Map<String, Integer> sortedWordsWithCount = sortMap(wordsWithCount);
        printMap(sortedWordsWithCount);
    }

    private Map<String, Integer> sortMap(Map<String, Integer> wordsWithCount) {
        WordCountComparator wordCountComparator = new WordCountComparator(wordsWithCount);
        Map<String, Integer> sortedWordsWithCount = new TreeMap<String, Integer>(wordCountComparator);
        sortedWordsWithCount.putAll(wordsWithCount);
        return sortedWordsWithCount;
    }

    private void printMap(Map<String, Integer> wordsWithCount) {
        for (String word : wordsWithCount.keySet()) {
            int count = wordsWithCount.get(word);
            outputStream.println(word + ": " + count);
        }
    }

    private class WordCountComparator implements Comparator<String> {

        Map comparedValues;

        private WordCountComparator(Map comparedValues) {
            this.comparedValues = comparedValues;
        }

        @Override
        public int compare(String firstKey, String secondKey) {
            Comparable firstValue = (Comparable) comparedValues.get(firstKey);
            Comparable secondValue = (Comparable) comparedValues.get(secondKey);

            if (!firstValue.equals(secondValue)) {
                return -firstValue.compareTo(secondValue);
            } else {
                return firstKey.compareTo(secondKey);
            }
        }
    }
}
