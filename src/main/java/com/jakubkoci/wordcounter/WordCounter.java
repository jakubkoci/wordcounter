package com.jakubkoci.wordcounter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kuba
 */
public class WordCounter {

    public Map<String, Integer> getWordsWithCount(String input) {
        String[] words = extractWords(input);
        return countWords(words);
    }

    private String[] extractWords(String input) {
        return input.split(" ");
    }

    private Map<String, Integer> countWords(String[] words) {
        Map<String, Integer> wordsWithCount = new HashMap<String, Integer>();
        for (String word : words) {
            int wordCount = 1;
            if (wordsWithCount.containsKey(word)) {
                wordCount = wordsWithCount.get(word) + 1;
            }
            wordsWithCount.put(word, wordCount);
        }
        return wordsWithCount;
    }
}
