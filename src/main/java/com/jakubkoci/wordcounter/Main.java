package com.jakubkoci.wordcounter;

import java.util.Map;

/**
 * @author kuba
 */
public class Main {

    public static void main(String[] args) {
        WordCounter wordCounter = new WordCounter();
        WordCountPrinter wordCountPrinter = new WordCountPrinter(System.out);

        String text = "ab aab aa bc aa a bb bbc cb bc aa a b";
        Map<String,Integer> wordsWithCount = wordCounter.getWordsWithCount(text);
        wordCountPrinter.print(wordsWithCount);
    }
}
